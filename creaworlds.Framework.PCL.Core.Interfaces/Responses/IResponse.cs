﻿using creaworlds.Framework.PCL.Core.Enumerators.Responses;
namespace creaworlds.Framework.PCL.Core.Interfaces.Responses
{
    public interface IResponse
    {
        int? CodeNumber { get; set; }
        Codes CodeName { get; set; }
        string Message { get; set; }
        int? ErrorNumber { get; set; }
    }
}
