﻿namespace creaworlds.Framework.PCL.Core.Interfaces.Responses
{
    public interface IResultable<obj> : IResponse
    {
        obj Result { get; set; }
    }
}
