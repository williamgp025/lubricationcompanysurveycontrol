﻿using creaworlds.Framework.PCL.Core.Interfaces.Responses;

namespace creaworlds.Framework.PCL.Core.Entities.Responses
{
    public sealed class ResponseEmpty : ResponseBase, IResponse
    {
    }
}
