﻿using creaworlds.Framework.PCL.Core.Interfaces.Responses;

namespace creaworlds.Framework.PCL.Core.Entities.Responses
{
    public sealed class ResponseObject<obj> : ResponseBase, IResponse, IResultable<obj>
    {
        public obj Result { get; set; }
    }
}
