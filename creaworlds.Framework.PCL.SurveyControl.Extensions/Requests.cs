﻿using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Users;
using System;

namespace creaworlds.Framework.PCL.SurveyControl.Extensions
{
    public static class Requests
    {
        public static object GetSignature<T>(this UserData extension, T content)
        {
            string token = Guid.NewGuid().ToString();
            string firma = string.Format("[{0}][{1}][{2}]"
                       , extension.UserName
                       , token
                       , Cryptography.ComputeSHA1(extension.ApiKey.ToUpper()));
            
            return new {
                UserName = extension.UserName,
                Token = token,
                Signature = Cryptography.ComputeSHA1(firma).ToLower(),
                Content = content
            };
        }
    }
}
