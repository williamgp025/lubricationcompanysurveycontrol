﻿using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Interfaces.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Users;

namespace creaworlds.Framework.PCL.SurveyControl.Extensions
{
    public static class Responses
    {
        public static void ConfigureInvalidUserName(this IResponse response, int errorNumber)
        {
            response.Configure(Codes.BadRequest, "Favor de especificar el Nombre de Usuario.", errorNumber);
        }
        public static void ConfigureInvalidPassword(this IResponse response, int errorNumber)
        {
            response.Configure(Codes.BadRequest, "Favor de especificar la contraseña del Ususario.", errorNumber);
        }

        public static bool ConfigureInvalidSignature(this IResponse response, UserData User)
        {
            bool flag = true;
            if (User == null)
            {
                response.ConfigureNullRequest(1400);
                flag = false;
            }
            else if (string.IsNullOrWhiteSpace(User.Name))
            {
                response.Configure(Codes.BadRequest, "Falta UserName", 1405);
                flag = false;

            }
            else if (string.IsNullOrWhiteSpace(User.ApiKey))
            {
                response.Configure(Codes.BadRequest, "Falta Apikey", 1410);
                flag = false;

            }

            return flag;
        }
    }
}
