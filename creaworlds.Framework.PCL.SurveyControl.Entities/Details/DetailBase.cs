﻿using SQLite;

namespace creaworlds.Framework.PCL.SurveyControl.Entities.Details
{
    public abstract class DetailBase
    {
        public string UserID { get; set; }
        public string EventID { get; set; }
        public string SurveyID { get; set; }
        public string RouteID { get; set; }
        public int ID { get; set; }
    }
}
