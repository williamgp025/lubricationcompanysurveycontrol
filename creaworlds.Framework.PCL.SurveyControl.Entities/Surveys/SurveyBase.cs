﻿namespace creaworlds.Framework.PCL.SurveyControl.Entities.Surveys
{
    public abstract class SurveyBase
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}
