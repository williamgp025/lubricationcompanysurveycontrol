﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.PCL.SurveyControl.Entities.Questions
{
    public sealed class QuestionUpdate : QuestionBase
    {
        public string ID { get; set; }
        public bool? Answer { get; set; }
        public string Comment { get; set; }
        public string EventID { get; set; }
    }
}
