﻿namespace creaworlds.Framework.PCL.SurveyControl.Entities.Questions
{
    public abstract class QuestionBase
    {
        public string ID { get; set; }
        public int Order { get; set; }
        public string Description { get; set; }
    }
}
