﻿namespace creaworlds.Framework.PCL.SurveyControl.Entities.Questions
{
    public sealed class QuestionDetailTable
    {
        public string QuestionDetailID { get; set; }
        public string RouteID { get; set; }
        public string SurveyID { get; set; }
    }
}
