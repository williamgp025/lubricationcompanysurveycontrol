﻿using SQLite;

namespace creaworlds.Framework.PCL.SurveyControl.Entities.Questions
{
    public sealed class QuestionTable : QuestionBase
    {
        public string SurveyID { get; set; }
        public string Help { get; set; }
        public bool IsMore { get; set; }
    }
}
