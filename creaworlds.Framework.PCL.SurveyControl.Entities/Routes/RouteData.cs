﻿using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;

namespace creaworlds.Framework.PCL.SurveyControl.Entities.Routes
{
    public sealed class RouteData : RouteBase
    {
        public QuestionData[] Question { get; set; }
    }
}
