﻿using System.Collections.Generic;

namespace creaworlds.Framework.PCL.SurveyControl.Entities.Routes
{
    public sealed class RouteUpdate
    {
        public List<RouteCollectionUpdate> Routes { get; set; }
    }
    public sealed class QuestionUpdate
    {
        public string ID { get; set; }
        public bool? Answer { get; set; }
        public string Comment { get; set; }
        public string EventID { get; set; }
    }
}
