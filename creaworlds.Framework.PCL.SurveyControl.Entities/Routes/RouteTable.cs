﻿using SQLite;

namespace creaworlds.Framework.PCL.SurveyControl.Entities.Routes
{
    public sealed class RouteTable : RouteBase
    {
        public string RouteID { get; set; }
        public bool Finish { get; set; }
        public string Comment { get; set; }
    }
}
