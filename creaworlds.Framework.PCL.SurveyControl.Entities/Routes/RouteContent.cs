﻿using creaworlds.Framework.PCL.SurveyControl.Entities.Details;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;
using creaworlds.Framework.PCL.SurveyControl.Entities.Surveys;

namespace creaworlds.Framework.PCL.SurveyControl.Entities.Routes
{
    public sealed class RouteContent
    {
        public DetailData[] Details { get; set; }
        public RouteData[] Routes { get; set; }
        public QuestionData[] Questions { get; set; }
        public SurveyData[] Surveys { get; set; }
    }
}