﻿namespace creaworlds.Framework.PCL.SurveyControl.Entities.Routes
{
    public abstract class RouteBase
    {
        public string ID { get; set; }
        public string Key { get; set; }
        public string Address { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
