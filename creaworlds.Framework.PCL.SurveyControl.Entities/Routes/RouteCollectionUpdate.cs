﻿using System.Collections.Generic;

namespace creaworlds.Framework.PCL.SurveyControl.Entities.Routes
{
    public sealed class RouteCollectionUpdate
    {
        public string ID { get; set; }
        public string Comments { get; set; }
        public List<QuestionUpdate> Questions { get; set; }
    }
}
