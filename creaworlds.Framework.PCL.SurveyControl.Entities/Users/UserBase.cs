﻿namespace creaworlds.Framework.PCL.SurveyControl.Entities.Users
{
    public abstract class UserBase
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string ID { get; set; }
        public string ApiKey { get; set; }
    }
}
