﻿namespace creaworlds.Framework.PCL.SurveyControl.Entities.Users
{
    public sealed class UserData : UserBase
    {
        public string UserName { get; set; }
    }
}
