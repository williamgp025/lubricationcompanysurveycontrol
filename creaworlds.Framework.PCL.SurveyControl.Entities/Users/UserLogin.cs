﻿namespace creaworlds.Framework.PCL.SurveyControl.Entities.Users
{
    public sealed class UserLogin : UserBase
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
