﻿using SQLite;

namespace creaworlds.Framework.PCL.SurveyControl.Entities.Answers
{
    public sealed class AnswerTable
    {
        [PrimaryKey,AutoIncrement]
        public int PK { get; set; }
        public bool Value { get; set; }
        public string QuestionDetailID { get; set; }
        public string Other { get; set; }
        public string QuestionID { get; set; }
    }
}
