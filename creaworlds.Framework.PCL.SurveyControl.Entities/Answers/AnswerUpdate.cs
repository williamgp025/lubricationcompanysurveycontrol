﻿namespace creaworlds.Framework.PCL.SurveyControl.Entities.Answers
{
    public sealed class AnswerUpdate : AnswerBase
    {
        public string ID { get; set; }
    }
}
