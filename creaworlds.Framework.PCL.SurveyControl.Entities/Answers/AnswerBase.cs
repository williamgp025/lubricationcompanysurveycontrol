﻿namespace creaworlds.Framework.PCL.SurveyControl.Entities.Answers
{
    public abstract class AnswerBase
    {
        public bool Value { get; set; }
        public string RouteID { get; set; }
    }
}
