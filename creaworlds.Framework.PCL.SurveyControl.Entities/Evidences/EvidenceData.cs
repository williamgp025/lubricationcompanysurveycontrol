﻿using System;

namespace creaworlds.Framework.PCL.SurveyControl.Entities.Evidences
{
    public sealed class EvidenceData : EvidenceBase
    {
        public string ID { get; set; }
        public byte[] FileContent { get; set; } = new byte[0];
        public DateTime? CreatedDate { get; set; }
    }
}
