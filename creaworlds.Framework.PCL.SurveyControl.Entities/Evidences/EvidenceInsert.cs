﻿namespace creaworlds.Framework.PCL.SurveyControl.Entities.Evidences
{
    public sealed class EvidenceInsert : EvidenceBase
    {
        public byte[] FileContent { get; set; }
        public decimal? ImgLatitude { get; set; }
        public decimal? ImgLongitude { get; set; }
    }
}
