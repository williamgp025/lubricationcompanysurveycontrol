﻿using SQLite;

namespace creaworlds.Framework.PCL.SurveyControl.Entities.Evidences
{
    public sealed class EvidenceTable
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string SurveyID { get; set; }
        public string QuestionDetailID { get; set; }
        public string FilePath { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public long ContentLength { get; set; }
    }
}
