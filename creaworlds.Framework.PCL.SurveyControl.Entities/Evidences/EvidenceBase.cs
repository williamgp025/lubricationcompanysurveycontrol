﻿namespace creaworlds.Framework.PCL.SurveyControl.Entities.Evidences
{
    public abstract class EvidenceBase
    {
        public string EventID { get; set; }
        public string FileName { get; set; }
        public long? ContentLength { get; set; }
        public string ContentType { get; set; }
    }
}
