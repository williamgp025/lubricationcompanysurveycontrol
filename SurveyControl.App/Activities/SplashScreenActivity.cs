﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using SurveyControl.App.Fragments;
using SurveyControl.App.Helpers;

namespace SurveyControl.App.Activities
{
    [Activity(Label = "@string/ApplicationName", MainLauncher = true, NoHistory = true, Theme = "@style/SplashTheme")]
    public class SplashScreenActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //confirmamos que hay un usuario en sesion
            if (Util.GetUserSession() != null)
            {
                StartActivity(new Intent(Application.Context, typeof(MainActivity)));
                Finish();
            }
            else
            {
                StartActivity(new Intent(Application.Context, typeof(LoginActivity)));
                Finish();
            }
        }

    }
}