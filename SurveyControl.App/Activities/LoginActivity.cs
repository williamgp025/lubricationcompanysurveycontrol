﻿using Android.App;
using Android.OS;
using SurveyControl.App.Fragments;

namespace SurveyControl.App.Activities
{
    [Activity(Label = "LoginActivity", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class LoginActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Main);

            FragmentTransaction ft = this.FragmentManager.BeginTransaction();
            FragmentLoginScreen fragmentlogin = new FragmentLoginScreen();

            ft.Add(Resource.Id.frameLayoutMain, fragmentlogin);
            ft.AddToBackStack(null);
            ft.Commit();
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();
            Finish();
        }
    }
}