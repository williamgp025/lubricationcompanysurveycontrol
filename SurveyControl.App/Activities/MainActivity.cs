﻿using Android.App;
using Android.OS;
using SurveyControl.App.Helpers;
using SurveyControl.App.Fragments.Routes;
using SurveyControl.App.Fragments.Profile;
using Android.Views;
using Android.Content;
using creaworlds.Framework.PCL.SurveyControl.DataAccess;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.SurveyControl.Entities.Users;
using System;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.SurveyControl.Entities.Evidences;
using System.Collections.Generic;
using Android.Widget;

namespace SurveyControl.App.Activities
{
    [Activity(Label = "SurveyControl.App", Icon = "@drawable/icon", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        UserData user;
        ProgressDialog progress;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            this.ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;
            ActionBar.Elevation = 1.0f;
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            //enable navigation mode to support tab layout
            AddTab("Usuario", new FragmentProfile());
            AddTab("Rutas", new FragmentRoute());
            user = Util.GetUserSession();
            progress = Util.ShowProgresDialog(this, "Sincronizado...");

        }
        public override void OnBackPressed()
        {
            if (this.FragmentManager.BackStackEntryCount > 0)
            {
                this.FragmentManager.PopBackStack();
            }
            else
            {
                Util.ShowAlertDialog(this, "Salir de la Aplicacion", "¿Desea salir de la aplicación?", "Aceptar", delegate
                {
                    Finish();
                }, "Cancelar");
            }
        }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.top_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.upload_top:
                    SyncData();
                    SendImageEvidence();
                    RouteUpdate();
                    break;
                case Resource.Id.logout_top:
                    SyncData();
                    SendImageEvidence();
                    Util.LogOut();
                    StartActivity(new Intent(this, typeof(LoginActivity)));
                    Finish();
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }
        public async void SendImageEvidence()
        {
            progress.Show();
            progress.SetMessage("Subiendo imagenes...");
            var imageEvidences = Evidence.List();
            if (imageEvidences.CodeName == Codes.OK)
            {
                EvidenceInsert[] evidence = new EvidenceInsert[imageEvidences.Result.Length];

                foreach (var item in imageEvidences.Result)
                {
                    await creaworlds.Framework.PCL.SurveyControl.Services.Evidence.UploadImage(item, user);
                }
                Toast.MakeText(this,"Proceso terminado",ToastLength.Short).Show();
            }
            progress.Hide();
        }

        public async void SyncData()
        {
            progress.Show();

            var rutas = Route.List(false, true);
            RouteUpdate routeupdate = new RouteUpdate();
            routeupdate.Routes = new List<RouteCollectionUpdate>();

            foreach (var ruta in rutas.Result)
            {
                var detalles = Detail.List(ruta.ID);
                var respuestas = Answer.List(detalles.Result.QuestionDetailID);
                List<QuestionUpdate> q = new List<QuestionUpdate>();
                foreach (var respuesta in respuestas.Result)
                {
                    q.Add(new QuestionUpdate
                    {
                        ID = respuesta.QuestionID,
                        Answer = respuesta.Value,
                        Comment = respuesta.Other,
                        EventID = respuesta.QuestionDetailID,
                    });
                }

                routeupdate.Routes.Add(new RouteCollectionUpdate
                {
                    Comments = ruta.Comment,
                    ID = ruta.ID,
                    Questions = q
                });
            }
            if (routeupdate.Routes.Count > 0)
                if (routeupdate.Routes[0].Questions.Count > 0)
                    await creaworlds.Framework.PCL.SurveyControl.Services.Route.Update(routeupdate, user);
            progress.Hide();
        }

        public async void RouteUpdate()
        {
            progress.Show();
            progress.SetMessage("Actualizando rutas");
            Helper.Reset();

            ResponseObject<RouteContent> rutas = await creaworlds.Framework.PCL.SurveyControl.Services.Route.List(user, new RouteList { UserID = user.ID });
            if (Util.NotifyHttpError(this, rutas))
            {
                //obtenemos datos de rutas
                var result_routes = Route.Insert(rutas.Result.Routes);
                var result_detail = Detail.Insert(rutas.Result.Details);
                var result_questions = Question.Insert(rutas.Result.Questions);
                var result_surveys = Survey.Insert(rutas.Result.Surveys);
                progress.Hide();
                if (result_surveys.CodeName == Codes.OK)
                {
                    Util.SetSessionDate("SessionDateSync", DateTime.Now.ToString());
                    Util.ShowAlertDialog(this, "Sincronizacion Exitosa", "Los datos se han sincronizado con el servidor.", "Aceptar");
                }
            }
            else
            {
                progress.Hide();
                Util.ShowAlertDialog(this, "Información", rutas.Message, "Aceptar");
            }
        }
        public void AddTab(string v, Fragment fragment)
        {
            var tab = ActionBar.NewTab();
            tab.SetText(v);
            tab.TabSelected += delegate (object sender, ActionBar.TabEventArgs e)
            {
                e.FragmentTransaction.Replace(Resource.Id.frameLayoutMain, fragment);
            };
            ActionBar.AddTab(tab);
        }
    }
}

