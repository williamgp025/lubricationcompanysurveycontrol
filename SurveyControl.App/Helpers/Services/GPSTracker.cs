﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;

namespace SurveyControl.App.Helpers.Services
{
    class GPSTracker : Service, ILocationListener
    {
        private Context context;
        bool isGPSEnabled = false;
        public bool isNetworkEnabled { get; set; } = false;
        public bool canGetLocation { get; set; } = false;
        Location location;
        public double longitude { get; set; }
        public double latitude { get; set; }

        private const long MIN_DISTANCE = 10;
        private const long MIN_TIME = 1000 * 60 * 1;

        protected LocationManager locationManager;

        public GPSTracker(Context context)
        {
            this.context = context;
            getLocation();
        }

        public Location getLocation()
        {
            try
            {
                locationManager = (LocationManager)context.GetSystemService(Service.LocationService);

                isGPSEnabled = locationManager.IsProviderEnabled(LocationManager.GpsProvider);

                isNetworkEnabled = locationManager.IsProviderEnabled(LocationManager.NetworkProvider);

                if (!isGPSEnabled && !isNetworkEnabled)
                {
                    ///no hay red
                }
                else
                {
                    canGetLocation = true;
                    if (isNetworkEnabled)
                    {
                        locationManager.RequestLocationUpdates(LocationManager.NetworkProvider, MIN_TIME, MIN_DISTANCE, this);
                        if (locationManager != null)
                        {
                            location = locationManager.GetLastKnownLocation(LocationManager.NetworkProvider);
                            if (location != null)
                            {
                                latitude = location.Latitude;
                                longitude = location.Longitude;
                            }
                        }
                    }
                    if (isGPSEnabled)
                    {
                        if (location == null)
                        {
                            locationManager.RequestLocationUpdates(LocationManager.GpsProvider, MIN_TIME, MIN_DISTANCE, this);
                            if (locationManager != null)
                            {
                                location = locationManager.GetLastKnownLocation(LocationManager.GpsProvider);
                                if (location != null)
                                {
                                    latitude = location.Latitude;
                                    longitude = location.Longitude;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return location;
        }

        public void StopUsingGPS()
        {
            if (locationManager != null)
            {
                locationManager.RemoveUpdates(this);
            }
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public void OnLocationChanged(Location location)
        {
        }

        public void OnProviderDisabled(string provider)
        {
        }

        public void OnProviderEnabled(string provider)
        {
        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {
        }
    }
}