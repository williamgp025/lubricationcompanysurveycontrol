﻿using Android.Content;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;

namespace SurveyControl.App.Helpers.Adapters
{
    public class RouteCustomAdapter : BaseAdapter<RouteTable>
    {
        RouteTable[] RutasA;
        Context Context;
        int ItemLayout, NameRoute, AddressRoute, StatusRoute, ImageViewMaps;

        public RouteCustomAdapter(Context context, RouteTable[]  rutas, int itemlayout, int nameroute, int addressroute, int statusroute, int imageviewmaps)
        {
            this.Context = context;
            this.RutasA = rutas;
            this.ItemLayout = itemlayout;
            this.NameRoute = nameroute;
            this.AddressRoute = addressroute;
            this.StatusRoute = statusroute;
            this.ImageViewMaps = imageviewmaps;
        }
        public override RouteTable this[int position]
        {
            get
            {
                return RutasA[position];
            }
        }

        public override int Count
        {
            get
            {
                return RutasA.Length;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var Item = RutasA[position];
            View ItemView;
            if (convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater)Context.GetSystemService(Context.LayoutInflaterService);
                ItemView = inflater.Inflate(ItemLayout, null);
            }
            else
            {
                ItemView = convertView;
            }

            ItemView.FindViewById<TextView>(NameRoute).Text = Item.Key;
            ItemView.FindViewById<TextView>(AddressRoute).Text = Item.Address;
            ItemView.FindViewById<TextView>(StatusRoute).Text =  Item.Finish ?  "Visitada" : "";
            ItemView.FindViewById<ImageView>(ImageViewMaps).Click += delegate
            {
                var gmmintent = Android.Net.Uri.Parse(string.Format("google.navigation:q={0},{1}&mode=d", Item.Latitude, Item.Longitude));
                Intent intent = new Intent(Intent.ActionView, gmmintent);

                PackageManager pm = Context.PackageManager;
                try
                {
                    pm.GetPackageInfo("com.google.android.apps.maps", 0);
                    if (pm != null)
                    {
                        intent.SetPackage("com.google.android.apps.maps");
                        Context.StartActivity(intent);
                    }
                }
                catch (PackageManager.NameNotFoundException e)
                {
                    Toast.MakeText(Context, "Google Maps don´t installed in this device.", ToastLength.Long).Show();
                }
            };
            return ItemView;
        }
    }
}