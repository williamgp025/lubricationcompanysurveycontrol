﻿using Android.Content;
using Android.Views;
using Android.Widget;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;
namespace SurveyControl.App.Helpers.Adapters
{
    class SurveyCustomAdapter : BaseAdapter<QuestionTable>
    {
        QuestionTable[] Preguntas;
        Context Context;
        int ItemLayoutTemplate, Number, Question, RadioGroup, InputMore;
        public SurveyCustomAdapter(Context context, QuestionTable[] preguntas, int itemlayouttemplate, int number, int question, int radiogroup, int inputMore)
        {
            Context = context;
            Preguntas = preguntas;
            ItemLayoutTemplate = itemlayouttemplate;
            Number = number;
            Question = question;
            RadioGroup = radiogroup;
            InputMore = inputMore;
        }
        public override QuestionTable this[int position]
        {
            get
            {
                return Preguntas[position];
            }
        }
        public override int Count
        {
            get
            {
                return Preguntas.Length;
            }
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var Item = Preguntas[position];
            View ItemView;
            //if (convertView == null)
            //{
                LayoutInflater inflater = (LayoutInflater)Context.GetSystemService(Context.LayoutInflaterService);
                ItemView = inflater.Inflate(ItemLayoutTemplate, null);
            //}
            //else
            //{
            //    ItemView = convertView;
            //}
            ItemView.FindViewById<EditText>(InputMore).Visibility = Item.IsMore ? ViewStates.Visible : ViewStates.Gone;
            ItemView.FindViewById<EditText>(InputMore).Hint = Item.Help;
            ItemView.FindViewById<TextView>(Number).Text = string.Format("Pregunta {0}", position + 1);
            ItemView.FindViewById<TextView>(Question).Text = string.Format("{0}", Item.Description);
            //var answer = Answer.List(Item.SurveyID);
            //if (answer.CodeNumber.Equals("OK"))
            //{
            //    ItemView.FindViewById<RadioGroup>(RadioGroup).Check(answer.Result.Value ? Resource.Id.radioButtonYesOption : Resource.Id.radioButtonNoOption);
            //}
            ItemView.FindViewById<RadioGroup>(RadioGroup).CheckedChange += (s, e) =>
            {
                var check = false;
                if (e.CheckedId == Resource.Id.radioButtonYesOption)
                {
                    check = true;
                }
                ItemView.FindViewById<RadioButton>(Resource.Id.radioButtonYesOption).Enabled = false;
                ItemView.FindViewById<RadioButton>(Resource.Id.radioButtonNoOption).Enabled = false;

                //Answer.Update(new AnswerTable
                //{
                //    ID = Item.SurveyID,
                //    QuestionDetailID = Item.QuestionID,
                //    Value = check
                //});
            };
            return ItemView;
        }
    }
}