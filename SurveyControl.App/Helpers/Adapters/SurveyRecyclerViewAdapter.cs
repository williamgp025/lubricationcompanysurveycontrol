﻿using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;
using creaworlds.Framework.PCL.SurveyControl.DataAccess;
using creaworlds.Framework.PCL.SurveyControl.Entities.Answers;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;

namespace SurveyControl.App.Helpers.Adapters
{
    public class SurveyRecyclerViewAdapter : RecyclerView.Adapter
    {
        public QuestionTable[] Preguntas;
        public string R;

        public SurveyRecyclerViewAdapter(QuestionTable[] preguntas, string r)
        {
            this.Preguntas = preguntas;
            R = r;
        }
        public override int ItemCount
        {
            get
            {
                return Preguntas.Length;
            }
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewholder, int position)
        {
            int pos = position;
            var item = Preguntas[position];
            var comment = string.Empty;
            var holder = viewholder as SurveyViewHolder;
            holder.InputAnswer.Visibility = item.IsMore ? ViewStates.Visible : ViewStates.Gone;
            holder.InputAnswer.Hint = item.Help;
            holder.SurveyAnswer.Visibility = item.Help.Length > 1 && item.IsMore ? ViewStates.Gone : ViewStates.Visible;
            holder.SurveyNumber.Text = string.Format("Pregunta {0}", position + 1);
            holder.SurveyQuestion.Text = string.Format("{0}", item.Description);
            var answer = Answer.Find(R, item.ID);
            holder.SurveyAnswer.SetTag(position, answer.Result.Value);

            var check = false;
            if (answer.CodeName == Codes.OK)
            {
                if (answer.Result.Value)
                {
                    holder.yesoption.Checked = true;
                }
                else
                {
                    holder.nooption.Checked = true;
                }
            }
            holder.SurveyAnswer.CheckedChange += (s, e) =>
            {
                if (e.CheckedId == Resource.Id.radioButtonYesOption)
                {
                    check = true;
                    holder.yesoption.GetTag(position);
                }
                else
                {
                    holder.nooption.GetTag(position);
                }
                if (answer.CodeName == Codes.OK)
                {
                    answer.Result.Other = comment;
                    answer.Result.Value = check;
                    Answer.Update(answer.Result);
                }
                else
                {
                    Answer.Insert(new AnswerTable
                    {
                        QuestionID = item.ID,
                        QuestionDetailID = comment,
                        Other = holder.InputAnswer.Text,
                        Value = check
                    });
                }
            };
            holder.InputAnswer.TextChanged += (s,e) =>
            {
                if (answer.CodeName == Codes.OK)
                {
                    answer.Result.Other = holder.InputAnswer.Text;
                    Answer.Update(answer.Result);
                }
                //else
                //{
                //    Answer.Insert(new AnswerTable
                //    {
                //        QuestionID = item.ID,
                //        QuestionDetailID = comment,
                //        Other = holder.InputAnswer.Text,
                //        Value = check
                //    });
                //    answer = Answer.Find(R, item.ID);
                //}
            };
        }
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var id = Resource.Layout.survey_card_layout;
            var ItemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);
            return new SurveyViewHolder(ItemView);
        }
    }
    public class SurveyViewHolder : RecyclerView.ViewHolder
    {
        public TextView SurveyNumber { get; set; }
        public TextView SurveyQuestion { get; set; }
        public RadioGroup SurveyAnswer { get; set; }
        public RadioButton yesoption { get; set; }
        public RadioButton nooption { get; set; }
        public EditText InputAnswer { get; set; }
        public SurveyViewHolder(View itemView) : base(itemView)
        {
            SurveyNumber = itemView.FindViewById<TextView>(Resource.Id.textViewSurveyNumber);
            SurveyQuestion = itemView.FindViewById<TextView>(Resource.Id.textViewSurveyQuestion);
            SurveyAnswer = ItemView.FindViewById<RadioGroup>(Resource.Id.radioGroupAnswers);
            InputAnswer = itemView.FindViewById<EditText>(Resource.Id.editTextMore);
            yesoption = itemView.FindViewById<RadioButton>(Resource.Id.radioButtonYesOption);
            nooption = itemView.FindViewById<RadioButton>(Resource.Id.radioButtonNoOption);
        }
    }
}