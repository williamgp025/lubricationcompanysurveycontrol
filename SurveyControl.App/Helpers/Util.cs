﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Widget;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.SurveyControl.Entities.Users;
using Newtonsoft.Json;
using System;

namespace SurveyControl.App.Helpers
{
    public static class Util
    {       
        public static Bitmap LoadAndResizeBitmap(this string fileName, int width, int height)
        {
            // First we get the the dimensions of the file on disk
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            BitmapFactory.DecodeFile(fileName, options);

            // Next we calculate the ratio that we need to resize the image by
            // in order to fit the requested dimensions.
            int outHeight = options.OutHeight;
            int outWidth = options.OutWidth;
            int inSampleSize = 1;

            if (outHeight > height || outWidth > width)
            {
                inSampleSize = outWidth > outHeight
                                   ? outHeight / height
                                   : outWidth / width;
            }

            // Now we will load the image and have BitmapFactory resize it for us.
            options.InSampleSize = inSampleSize;
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile(fileName, options);

            return resizedBitmap;
        }

        public static void ShowAlertDialog(Context context, string title, string message, string ok, EventHandler<DialogClickEventArgs> action = null, string cancel = null)
        {
            if (message == null) message = "Error de Conexión...";
            AlertDialog.Builder alert2 = new AlertDialog.Builder(context);
            if (title != null)
                alert2.SetTitle(title);
            alert2.SetMessage(message);
            alert2.SetPositiveButton(ok, action);
            if (cancel != null)
                alert2.SetNegativeButton(cancel, delegate { });
            AlertDialog dialog2 = alert2.Create();
            alert2.Show();
        }

        public static ProgressDialog ShowProgresDialog(Context context, string message, bool cancelable = false, bool indeterminate = true)
        {
            ProgressDialog progress = new ProgressDialog(context);
            progress = new Android.App.ProgressDialog(context);
            progress.Indeterminate = indeterminate;
            progress.SetProgressStyle(Android.App.ProgressDialogStyle.Spinner);
            progress.SetMessage(message);
            progress.SetCancelable(cancelable);
            return progress;
        }

        public static bool NotifyHttpError<T>(Context context, ResponseObject<T> response)
        {
            bool flag = false;
            if (response.CodeName == Codes.OK)
                flag = true;
            else if (response.CodeName == Codes.Found)
                flag = true;
            else if (response.CodeName.Equals("NoRedHttp"))
                Toast.MakeText(context, "Sin acceso a Internet", ToastLength.Long).Show();
            else if (response.CodeName == Codes.InternalServerError)
                Toast.MakeText(context, 
                    string.Format("{0} / {1}", response.Message, response.ServerName), 
                    ToastLength.Long).Show();
            return flag;
        }

        public static void SetUserInSession(UserData user)
        {
            SetSessionDate("CustomerDataInSession", JsonConvert.SerializeObject(user));
        }

        private static ISharedPreferences GetPreferences()
        {
            return Application.Context.GetSharedPreferences("CustomerDataSessionPreferences", FileCreationMode.Private);
        }

        public static void SetSessionDate(string key, string data)
        {
            ISharedPreferencesEditor edit = GetPreferences().Edit();

            edit.PutString(key, data);
            edit.Commit();
        }

        public static void LogOut()
        {
            ISharedPreferencesEditor edit = GetPreferences().Edit();

            edit.PutString("UserSession", "");
            edit.Commit();
        }

        public static void SetSessionLog()
        {
            ISharedPreferencesEditor edit = GetPreferences().Edit();
            edit.PutBoolean("UserLog", true);
            edit.Commit();
        }

        public static bool GetUserSession(string key)
        {

            var tem = GetPreferences().GetBoolean(key, false);
            return tem;
        }

        public static UserData GetUserSession()
        {
            UserData usersession = null;
            var user = GetPreferences().GetString("UserSession", "");
            if (!string.IsNullOrWhiteSpace(user))
            {
                usersession = new UserData();
                JsonConvert.PopulateObject(user, usersession);
            }
            return usersession; 
        }

        public static string GetSyncDate()
        {
            return GetPreferences().GetString("SessionDateSync", "No sincronizado.");            
        }
    }
}