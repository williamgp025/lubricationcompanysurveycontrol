﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using creaworlds.Framework.PCL.SurveyControl.Services;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.SurveyControl.Entities.Users;
using SurveyControl.App.Helpers;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;
using Newtonsoft.Json;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using Android.Content;
using SurveyControl.App.Activities;
using creaworlds.Framework.PCL.SurveyControl.DataAccess;

namespace SurveyControl.App.Fragments
{
    public class FragmentLoginScreen : Fragment
    {
        ProgressDialog progress;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.fragment_login_layout, container, false);
            this.Activity.ActionBar.Title = "Iniciar de sesión";
            progress = Util.ShowProgresDialog(Activity, "Iniciando sesión...");
            EditText UserName = view.FindViewById<EditText>(Resource.Id.editTextUser);
            EditText Password = view.FindViewById<EditText>(Resource.Id.editTextPassword);
            Button buttonlogin = view.FindViewById<Button>(Resource.Id.buttonLogin);
            buttonlogin.Click += async delegate
            {
                progress.Show();               
                //logueamos al usuario
                ResponseObject<UserData> response = await User.Login(UserName.Text, Password.Text);
                if (Util.NotifyHttpError(Activity, response))
                {
                    //Util.SetSessionLog();
                    //guardamos sesion
                    progress.SetMessage("Buscando rutas...");
                    Util.SetSessionDate("UserSession", JsonConvert.SerializeObject(response.Result));
                    ResponseObject<RouteContent> rutas = await creaworlds.Framework.PCL.SurveyControl.Services.Route.List(response.Result, new RouteList { UserID = response.Result.ID });
                    if (Util.NotifyHttpError(Activity, rutas))
                    {
                        Helper.Reset();
                        progress.SetMessage("Cargando rutas...");
                        //obtenemos datos de rutas
                        var result_routes = creaworlds.Framework.PCL.SurveyControl.DataAccess.Route.Insert(rutas.Result.Routes);
                        var result_detail = Detail.Insert(rutas.Result.Details);
                        var result_questions = creaworlds.Framework.PCL.SurveyControl.DataAccess.Question.Insert(rutas.Result.Questions);
                        var result_surveys = Survey.Insert(rutas.Result.Surveys);

                        if (result_surveys.CodeName == Codes.OK)
                        {
                            //lanzamos actividad
                            StartActivity(new Intent(Activity, typeof(MainActivity)));
                        }
                        else
                        {
                            Util.ShowAlertDialog(Activity, "Error", result_surveys.Message, "Aceptar");
                        }
                    }
                    else
                    {
                        Util.ShowAlertDialog(Activity, "Error", rutas.Message, "Aceptar");
                    }
                    progress.Hide();
                }
                else
                {
                    progress.Hide();
                    Util.ShowAlertDialog(Activity, "Error", response.Message, "Aceptar");
                }
            };
            return view;
        }
    }
}