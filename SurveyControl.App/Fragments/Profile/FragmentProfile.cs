﻿using Android.App;
using Android.Gms.Maps;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Gms.Maps.Model;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;
using System;
using System.Linq;
using creaworlds.Framework.PCL.SurveyControl.Entities.Users;
using SurveyControl.App.Helpers;
using creaworlds.Framework.PCL.SurveyControl.DataAccess;

namespace SurveyControl.App.Fragments.Profile
{
    public class FragmentProfile : Fragment, IOnMapReadyCallback
    {
        private GoogleMap Gmap;
        private MapFragment mapfragment;
        TextView FullName, UserName, SyncDate, RutasVisitadas;
        UserData user;
        RouteTable[] rutas;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        private void SetuMap()
        {
            mapfragment = FragmentManager.FindFragmentByTag("map") as MapFragment; ;
            FragmentTransaction ft = FragmentManager.BeginTransaction();
            mapfragment = MapFragment.NewInstance();
            ft.Add(Resource.Id.google_maps, mapfragment, "map");
            ft.Commit();
            mapfragment.GetMapAsync(this);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.fragment_profile_layout, container, false);
            this.Activity.ActionBar.Title = "Perfil del usuario";
            SetuMap();
            FullName = view.FindViewById<TextView>(Resource.Id.textViewFullName);
            UserName = view.FindViewById<TextView>(Resource.Id.textViewUserName);
            SyncDate = view.FindViewById<TextView>(Resource.Id.textViewSyncDate);
            RutasVisitadas = view.FindViewById<TextView>(Resource.Id.textViewVisit);
            var progress = Util.ShowProgresDialog(Activity, "Cargando Datos Usuario");
            progress.Show();
            user = Util.GetUserSession();
            if (user != null)
            {
                rutas = Route.List(true).Result;
                FullName.Text = user.FullName;
                UserName.Text = user.Name;
                SyncDate.Text = string.Format(SyncDate.Text, Util.GetSyncDate());
                var visitadas = rutas.Where(r => r.Finish).ToArray().Length;
                RutasVisitadas.Text = string.Format(RutasVisitadas.Text, visitadas, rutas.Length);
            }
     
            progress.Hide();
            return view;
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            this.Gmap = googleMap;
            var incomplete_color = BitmapDescriptorFactory.HueRed;
            var complete_color = BitmapDescriptorFactory.HueGreen;
            Gmap.UiSettings.ZoomControlsEnabled = true;
            Gmap.MyLocationEnabled = true;

            LatLng location = new LatLng(19.5230833, -91.5502491);
            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            builder.Target(location);
            //builder.Zoom(18);
            //builder.Bearing(155);
            builder.Tilt(20);
            CameraPosition cameraPosition = builder.Build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
            Gmap.MoveCamera(cameraUpdate);

            foreach (var l in rutas)
            {
                Gmap.AddMarker(new MarkerOptions()
                    .SetPosition(new LatLng((double)l.Latitude, (double)l.Longitude))
                    .SetTitle(l.Key)
                    .SetSnippet(l.Address + " " + (l.Finish ? "Visitada" : "No Visitada"))
                    .SetIcon(BitmapDescriptorFactory.DefaultMarker(l.Finish ? complete_color : incomplete_color)));
            }
        }
    }
}