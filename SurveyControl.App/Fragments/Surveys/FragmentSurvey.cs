﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using SurveyControl.App.Helpers.Adapters;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;
using creaworlds.Framework.PCL.SurveyControl.DataAccess;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.SurveyControl.Entities.Answers;

namespace SurveyControl.App.Fragments.Surveys
{
    public class FragmentSurvey : Fragment
    {
        //RecyclerView recyclerview;
        //RecyclerView.LayoutManager mlayoutmanager;
        //SurveyRecyclerViewAdapter adapter;
        ListView listviewsurvey;
        SurveyCustomAdapter adapter;
        Button FinishSurvey, NextQuestion;
        TextView SurveyNumber;
        TextView SurveyQuestion;
        RadioGroup SurveyAnswer;
        RadioButton yesoption;
        RadioButton nooption;
        EditText InputAnswer;
        string id, name;
        ResponseObject<QuestionDetailTable> detalles;
        ResponseObject<QuestionTable[]> preguntas;
        int cont;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Create your fragment here
            id = Arguments.GetString("RouteID");
            name = Arguments.GetString("RouteName");
            detalles = Detail.List(id);
            preguntas = Question.List(new QuestionTableList { SurveyID = detalles.Result.SurveyID });
            this.Activity.ActionBar.Title = name;
            cont = 0;
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.survey_card_layout, container, false);
            //FinishSurvey = view.FindViewById<Button>(Resource.Id.buttonFinishSurvey);
            SurveyNumber = view.FindViewById<TextView>(Resource.Id.textViewSurveyNumber);
            SurveyQuestion = view.FindViewById<TextView>(Resource.Id.textViewSurveyQuestion);
            SurveyAnswer = view.FindViewById<RadioGroup>(Resource.Id.radioGroupAnswers);
            InputAnswer = view.FindViewById<EditText>(Resource.Id.editTextMore);
            yesoption = view.FindViewById<RadioButton>(Resource.Id.radioButtonYesOption);
            nooption = view.FindViewById<RadioButton>(Resource.Id.radioButtonNoOption);
            NextQuestion = view.FindViewById<Button>(Resource.Id.buttonNextQuestion);
            if (cont >= preguntas.Result.Length)
            {
                FragmentTransaction ft = this.FragmentManager.BeginTransaction();
                FragmentSendData fragmentsenddata = new FragmentSendData();
                ft.Replace(Resource.Id.frameLayoutMain, fragmentsenddata);
                var bundle = new Bundle();
                bundle.PutString("RouteID", id);
                bundle.PutString("RouteName", name);
                fragmentsenddata.Arguments = bundle;
                ft.AddToBackStack(null);
                ft.Commit();
            }
            else
            {
                SurveyNumber.Text = string.Format("Pregunta {0}", cont + 1);
                SurveyQuestion.Text = preguntas.Result[cont].Description;
                InputAnswer.Visibility = preguntas.Result[cont].IsMore ? ViewStates.Visible : ViewStates.Gone;
                InputAnswer.Hint = preguntas.Result[cont].Help;
                SurveyAnswer.Visibility = preguntas.Result[cont].Help.Length > 1 && preguntas.Result[cont].IsMore ? ViewStates.Gone : ViewStates.Visible;
                SurveyQuestion.Text = string.Format("{0}", preguntas.Result[cont].Description);
                var check = false;
                var answer = new AnswerTable();

                SurveyAnswer.CheckedChange += (s, e) =>
                {
                    NextQuestion.Enabled = true;
                };
                InputAnswer.TextChanged += (s, e) =>
                {
                    NextQuestion.Enabled = true;
                };
                NextQuestion.Click += delegate
                {

                    if (yesoption.Checked)
                    {
                        check = true;
                    }
                    answer.QuestionID = preguntas.Result[cont].ID;
                    answer.QuestionDetailID = detalles.Result.QuestionDetailID;
                    answer.Other = InputAnswer.Text;
                    answer.Value = check;
                    Answer.Insert(answer);
                    InputAnswer.Text = "";
                    nooption.Checked = false;
                    yesoption.Checked = false;

                    cont++;

                    FragmentTransaction ft = this.FragmentManager.BeginTransaction();
                    ft.Detach(this);
                    ft.Attach(this);
                    var bundle = new Bundle();
                    ft.Commit();
                };
            }
            //adapter = new SurveyRecyclerViewAdapter(preguntas.Result, id);
            //adapter = new SurveyCustomAdapter(Activity, preguntas.Result, Resource.Layout.survey_card_layout, Resource.Id.textViewSurveyNumber, Resource.Id.textViewSurveyQuestion, Resource.Id.radioGroupAnswers, Resource.Id.editTextMore);
            //recyclerview = view.FindViewById<RecyclerView>(Resource.Id.recyclerViewSurvey);
            //recyclerview.SetAdapter(adapter);
            //mlayoutmanager = new LinearLayoutManager(Context, LinearLayoutManager.Vertical, false);
            //recyclerview.SetLayoutManager(mlayoutmanager);
            //listviewsurvey = view.FindViewById<ListView>(Resource.Id.listViewSurvey);
            //listviewsurvey.Adapter = adapter;
            //FinishSurvey.Click += delegate
            //{
            //    FragmentTransaction ft = this.FragmentManager.BeginTransaction();
            //    FragmentSendData fragmentsenddata = new FragmentSendData();
            //    ft.Replace(Resource.Id.frameLayoutMain, fragmentsenddata);
            //    var bundle = new Bundle();
            //    bundle.PutString("RouteID", id);
            //    fragmentsenddata.Arguments = bundle;
            //    ft.AddToBackStack(null);
            //    ft.Commit();
            //};
            return view;
        }     
    }
}