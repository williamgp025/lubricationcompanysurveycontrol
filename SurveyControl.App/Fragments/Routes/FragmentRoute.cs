﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;
using SurveyControl.App.Helpers;
using SurveyControl.App.Helpers.Adapters;
using SurveyControl.App.Fragments.Surveys;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;
using creaworlds.Framework.PCL.SurveyControl.DataAccess;

namespace SurveyControl.App.Fragments.Routes
{
    public class FragmentRoute : Fragment
    {
        public RouteTable[] Rutas;
        RouteCustomAdapter adapter;
        ListView lista;
        ProgressDialog progress;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.fragment_route_layout, container, false);
            this.Activity.ActionBar.Title = "Rutas";
            progress = Util.ShowProgresDialog(Activity, "Cargando rutas...");
            ///Obtener Rutas
            try
            {
                progress.Show();
                ///obtenemos los datos de la base de datos local
                Rutas = Route.List().Result;
                progress.Hide();
            }
            catch (Exception)
            {
                progress.Hide();
            }
            adapter = new RouteCustomAdapter(Activity, Rutas, Resource.Layout.list_routes_item, Resource.Id.textViewRoute, Resource.Id.textViewAddress, Resource.Id.textViewDetail, Resource.Id.imageViewMaps);
            lista = view.FindViewById<ListView>(Resource.Id.listViewRutas);
            lista.Adapter = adapter;
            lista.ItemClick += OnItemClick;
            return view;
        }

        public void OnItemClick(object sender, AdapterView.ItemClickEventArgs e)
         {
            progress.SetMessage("Cargando Preguntas");
            progress.Show();
            try
            {
                FragmentTransaction ft = this.FragmentManager.BeginTransaction();
                FragmentSurvey fragmentsurvey = new FragmentSurvey();
                ft.Replace(Resource.Id.frameLayoutMain, fragmentsurvey);
                ft.AddToBackStack(null);
                var bundle = new Bundle();
                bundle.PutString("RouteID", Rutas[e.Position].ID);
                bundle.PutString("RouteName", Rutas[e.Position].Key);
                fragmentsurvey.Arguments = bundle;
                ft.Commit();
                progress.Hide();
            }
            catch (Exception)
            {
                progress.Hide();
            }
        }
    }
}