﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Provider;
using Java.IO;
using SurveyControl.App.Helpers;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;
using SurveyControl.App.Fragments.Routes;
using creaworlds.Framework.PCL.SurveyControl.DataAccess;
using creaworlds.Framework.PCL.SurveyControl.Entities.Evidences;
using SurveyControl.App.Helpers.Services;
using Android.Gms.Maps.Model;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;

namespace SurveyControl.App.Fragments
{
    public class FragmentSendData : Fragment
    {
        public Button TakeEvidence1, TakeEvidence2, TakeEvidence3, SendButton;
        public ImageView Evidence1, Evidence2, Evidence3;
        public EditText Comment;
        public static File file;
        public static File dir;
        public static Bitmap bitmap;
        public int flag = 0;
        public string id;
        public string name;
        public LatLng ubication;
        private QuestionDetailTable Event;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.fragment_senddata_layout, container, false);
            this.Activity.ActionBar.Title = "Enviar Evidencia";
            TakeEvidence1 = view.FindViewById<Button>(Resource.Id.buttonEvidencia1);
            TakeEvidence2 = view.FindViewById<Button>(Resource.Id.buttonEvidencia2);
            TakeEvidence3 = view.FindViewById<Button>(Resource.Id.buttonEvidencia3);
            Comment = view.FindViewById<EditText>(Resource.Id.editTextComments);
            SendButton = view.FindViewById<Button>(Resource.Id.buttonSendEvidence);

            Evidence1 = view.FindViewById<ImageView>(Resource.Id.imageViewEvidence1);
            Evidence2 = view.FindViewById<ImageView>(Resource.Id.imageViewEvidence2);
            Evidence3 = view.FindViewById<ImageView>(Resource.Id.imageViewEvidence3);
            id = Arguments.GetString("RouteID");
            name = Arguments.GetString("RouteName");

            SendButton.Click += delegate
            {
                Route.Update(new RouteTable
                {
                    RouteID = id,
                    Comment = Comment.Text,
                    Finish = true
                });
                Util.ShowAlertDialog(Activity, "Encuesta terminada", "La encuesta " + name + " ha sido finalizada con exito", "Aceptar");
                FragmentTransaction ft = this.FragmentManager.BeginTransaction();
                FragmentRoute fragmentroute = new FragmentRoute();
                ft.Replace(Resource.Id.frameLayoutMain, fragmentroute);
                //ft.AddToBackStack(null);
                ft.Commit();
            };

            TakeEvidence1.Click += delegate
            {
                TakePhotoEvidence(Evidence1, 1);
            };
            TakeEvidence2.Click += delegate
            {
                TakePhotoEvidence(Evidence2, 2);
            };
            TakeEvidence3.Click += delegate
            {
                TakePhotoEvidence(Evidence3, 3);
            };
            return view;
        }

        private void CreateDirectory()
        {
            dir = new File(Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryPictures), "SurveyControl");
            if (!dir.Exists())
            {
                dir.Mkdirs();
            }
        }
        private void TakePhotoEvidence(ImageView evidence, int code)
        {
            CreateDirectory();
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            file = new File(dir, string.Format("SurveyControl_{0}{1}.jpg", Guid.NewGuid(), code));
            intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(file));
            StartActivityForResult(intent, code);
        }
        public override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            var gps = new GPSTracker(Activity);
            if (gps.canGetLocation)
            {
                ubication = new LatLng(gps.latitude, gps.longitude);
                //Toast.MakeText(Activity, "Your Location is - \nLat: " + ubication.Latitude+ "\nLong: " + ubication.Latitude, ToastLength.Long).Show();
            }
            //else
            //{
            //    Toast.MakeText(Activity, "Can´t get Location", ToastLength.Long).Show();
            //}
            Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
            Android.Net.Uri contentUri = Android.Net.Uri.FromFile(file);
            mediaScanIntent.SetData(contentUri);
            Activity.SendBroadcast(mediaScanIntent);

            int height = Resources.DisplayMetrics.HeightPixels;
            int width = Evidence1.Height;
            bitmap = Util.LoadAndResizeBitmap(file.Path, width, height);
            if (bitmap != null)
            {
                switch (requestCode)
                {
                    case 1:
                        Evidence1.SetImageBitmap(bitmap);
                        CreateImageInDB();
                        flag++;
                        break;
                    case 2:
                        Evidence2.SetImageBitmap(bitmap);
                        CreateImageInDB();
                        flag++;
                        break;
                    case 3:
                        Evidence3.SetImageBitmap(bitmap);
                        CreateImageInDB();
                        flag++;
                        break;
                }
                bitmap = null;
            }
            if (flag >= 3)
            {
                SendButton.Text = "Finalizar";
                SendButton.Enabled = true;
            }
            GC.Collect();
        }

        private void CreateImageInDB() {
            if (Event == null)
                Event = Detail.Find(id).Result;

            Evidence.Insert(new EvidenceTable
            {
                SurveyID = Event.SurveyID,
                QuestionDetailID = Event.QuestionDetailID,
                FilePath = file.Path,
                ContentLength = new System.IO.FileInfo(file.Path).Length,
                Latitude = Convert.ToDecimal(ubication.Latitude),
                Longitude = Convert.ToDecimal(ubication.Longitude)
            });
        }
    }
}