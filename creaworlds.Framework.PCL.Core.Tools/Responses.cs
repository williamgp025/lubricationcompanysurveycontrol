﻿using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Interfaces.Responses;

namespace creaworlds.Framework.PCL.Core.Tools
{
    public static class Responses
    {
        public static void Configure(this IResponse sender, Codes code)
        {
            sender.CodeName = code;
        }
        public static void Configure(this IResponse sender, Codes code, string message, int? errorNumber)
        {
            sender.CodeName = code;
            sender.Message = message;
            sender.ErrorNumber = errorNumber;
        }
        public static void ConfigureNullRequest(this IResponse sender, int errorNumber)
        {
            sender.Configure(Codes.BadRequest, "No se han especificado los parametros requeridos para realizar la operación o son invalidos.", errorNumber);
        }
        public static void ConfigureNullContent(this IResponse sender, int errorNumber)
        {
            sender.Configure(Codes.BadRequest, "No se han especificado el contenido de la solicitud o es invalida.", errorNumber);
        }
        public static void ConfigureNullAuthorID(this IResponse sender, int errorNumber)
        {
            sender.Configure(Codes.BadRequest, "No se ha especificado el ID del usuario que realiza la operación o es invalido.", errorNumber);
        }
        public static void ConfigureConnectionFailure(this IResponse sender)
        {
            sender.Configure(Codes.ServiceUnavailable, "Ha ocurrido un error al conectar con la capa de datos.", 1501);
        }
        public static void ConfigureTranBeginFailure(this IResponse sender)
        {
            sender.Configure(Codes.InternalServerError, "Ha ocurrido un error al iniciar la operación con la capa de datos.", 1502);
        }
        public static void ConfigureTranCommitFailure(this IResponse sender)
        {
            sender.Configure(Codes.InternalServerError, "Ha ocurrido un error al confirmar la operación con la capa de datos.", 1503);
        }
        public static void ConfigureUnauthorized(this IResponse sender, int errorNumber)
        {
            sender.Configure(Codes.Unauthorized, "No se han especificado las credenciales de la solicitud o son invalidos.", errorNumber);
        }
        public static ResponseObject<T> ToResponseObject<T>(this IResponse sender, T result = null) where T : class
        {
            return new ResponseObject<T>
            {
                CodeName = sender.CodeName,
                Message = sender.Message,
                ErrorNumber = sender.ErrorNumber,
                Result = result
            };
        }
    }
}
