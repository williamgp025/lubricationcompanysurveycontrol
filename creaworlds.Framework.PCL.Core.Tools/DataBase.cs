﻿using Android.Util;
using creaworlds.Framework.PCL.SurveyControl.Entities.Answers;
using creaworlds.Framework.PCL.SurveyControl.Entities.Evidences;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;
using creaworlds.Framework.PCL.SurveyControl.Entities.Surveys;
using SQLite;
using System;
using System.IO;

namespace creaworlds.Framework.PCL.Core.Tools
{
    public sealed class DataBase :IDisposable 
    {
        public SQLiteConnection Connection
        {
            get
            {
                return new SQLiteConnection(Path.Combine(FileDirectory, DbName));
            }
        }

        public string DbName { get; set; } = "SurveyControl.db";

        public string FileDirectory { get; set; } = "/data/user/0/com.cimpsa.akron/files";
                
        public void Dispose()
        {
            CloseConnection();
        }

        public void CloseConnection()
        {
            Connection.Close();
        }

        public bool OpenConnection()
        {
            var connection = new SQLiteConnection(Path.Combine(FileDirectory, DbName));
            try
            {
                using (connection)
                {
                    ////tablas para crear
                    connection.CreateTable<RouteTable>();
                    connection.CreateTable<AnswerTable>();
                    connection.CreateTable<QuestionDetailTable>();
                    connection.CreateTable<QuestionTable>();
                    connection.CreateTable<SurveyTable>();
                    connection.CreateTable<EvidenceTable>();
                    return true;
                }
            }
            catch (SQLiteException e)
            {
                Log.Info("SQLiteException", e.Message);
                return false;
            }
        }
    }
}
