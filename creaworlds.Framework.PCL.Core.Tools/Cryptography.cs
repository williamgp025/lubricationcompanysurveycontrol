﻿using PCLCrypto;
using System;
using System.Text;

namespace creaworlds.Framework.PCL.Core.Tools
{
    public static class Cryptography
    {
        public static string ComputeSHA1(string text)
        {
            try
            {
                var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(HashAlgorithm.Sha1);
                byte[] inputBytes = Encoding.UTF8.GetBytes(text);
                byte[] hash = hasher.HashData(inputBytes);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }
                return sb.ToString();
            }
            catch (Exception e) {                
                return null;
            }
        }
    }
}
