﻿using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.PCL.Core.Tools
{
    public static class Service
    {
        private static string url = "http://akronws.cimpsa.com.mx/{0}/{1}";
        public static ResponseObject<T> GetDataRest<T>(object data, string controller, string method)
        {
            ResponseObject<T> ro = new ResponseObject<T>();
            try
            {
                string urlbase = string.Format(url, controller, method);
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string json = JsonConvert.SerializeObject(data);
                    HttpResponseMessage res = client.PostAsync(urlbase,
                        new StringContent(json, Encoding.UTF8, "application/json")).Result;
                    var tem = res.Content.ReadAsStringAsync().Result;
                    JsonConvert.PopulateObject(tem, ro);
                }
            }
            catch (Exception e)
            {
                //Debug.WriteLine(e);
                if (ro.ErrorNumber == 0 && ro.CodeNumber == 0)
                    ro.Configure(Codes.NotImplemented, e.Message, 1501);
            }

            return ro;
        }
        public static ResponseEmpty GetDataRest(object data, string controller, string method)
        {
            ResponseEmpty ro = new ResponseEmpty();
            try
            {
                string urlbase = string.Format(url, controller, method);
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string json = JsonConvert.SerializeObject(data);
                    HttpResponseMessage res = client.PostAsync(urlbase,
                        new StringContent(json, Encoding.UTF8, "application/json")).Result;
                    var tem = res.Content.ReadAsStringAsync().Result;
                    JsonConvert.PopulateObject(tem, ro);
                }
            }
            catch (Exception e)
            {
                //Debug.WriteLine(e);
                if (ro.ErrorNumber == 0 && ro.CodeNumber == 0)
                    ro.Configure(Codes.NotImplemented, e.Message, 1501);
            }

            return ro;
        }
        public static async Task<ResponseObject<T>> GetDataRestAsync<T>(object data, string controller, string method)
        {
            ResponseObject<T> ro = new ResponseObject<T>();
            try
            {
                string urlbase = string.Format(url, controller, method);
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string json = JsonConvert.SerializeObject(data);

                    using (HttpResponseMessage res = await client.PostAsync(urlbase, new StringContent(json, Encoding.UTF8, "application/json")))
                    {
                        var tem = res.Content.ReadAsStringAsync().Result;

                        JsonConvert.PopulateObject(tem, ro);
                    }
                }
            }
            catch (Exception e)
            {
                //Debug.WriteLine(e);
                if (ro.ErrorNumber == 0 && ro.CodeNumber == 0)
                    ro.Configure(Codes.NotImplemented, e.Message, 1501);
            }

            return ro;
        }
        public static async Task<ResponseEmpty> GetDataRestAsync(object data, string controller, string method)
        {
            ResponseEmpty ro = new ResponseEmpty();
            try
            {
                string urlbase = string.Format(url, controller, method);
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string json = JsonConvert.SerializeObject(data);

                    using (HttpResponseMessage res = await client.PostAsync(urlbase, new StringContent(json, Encoding.UTF8, "application/json")))
                    {
                        var tem = res.Content.ReadAsStringAsync().Result;

                        JsonConvert.PopulateObject(tem, ro);
                    }
                }
            }
            catch (Exception e)
            {
                //Debug.WriteLine(e);
                if (ro.ErrorNumber == 0 && ro.CodeNumber == 0)
                    ro.Configure(Codes.NotImplemented, e.Message, 1501);
            }

            return ro;
        }

    }
}
