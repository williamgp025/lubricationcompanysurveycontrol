﻿using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Answers;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;
using creaworlds.Framework.PCL.SurveyControl.Entities.Surveys;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public static partial class Helper
    {
        public static ResponseEmpty Reset()
        {
            ResponseEmpty response = new ResponseEmpty();
            using (var db = new DataBase())
            {
                if (!db.OpenConnection())
                {
                    response.ConfigureConnectionFailure();
                }
                else
                {
                    try
                    {
                        db.Connection.DeleteAll<RouteTable>();
                        db.Connection.DeleteAll<QuestionDetailTable>();
                        db.Connection.DeleteAll<SurveyTable>();
                        db.Connection.DeleteAll<AnswerTable>();
                        db.Connection.DeleteAll<QuestionTable>();
                        response.Configure(Codes.OK);
                    }
                    catch (SQLiteException e)
                    {
                        response.Configure(Codes.Declined, e.Message, 1500);
                    }
                }
            }

            return response;
        }
    }
}
