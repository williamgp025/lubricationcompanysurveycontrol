﻿using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;
using SQLite;
using System.Linq;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public partial class Detail
    {


        public static ResponseObject<QuestionDetailTable> Find(string request)
        {
            ResponseObject<QuestionDetailTable> response = new ResponseObject<QuestionDetailTable>();
            if (request == null)
            {
                response.ConfigureNullRequest(1400);
            }
            else
            {
                var db = new DataBase();
                if (!db.OpenConnection())
                {
                    response.ConfigureConnectionFailure();
                }
                else
                {
                    try
                    {
                        var tem = from t1 in db.Connection.Table<QuestionDetailTable>().ToList()
                                  where t1.RouteID.Equals(request)
                                  select t1;

                        response.Result = tem.FirstOrDefault();
                        response.Configure(Codes.OK);
                    }
                    catch (SQLiteException e)
                    {
                        response.Configure(Codes.Declined, e.Message, 1500);
                    }
                }
            }
            return response;
        }
    }
}
