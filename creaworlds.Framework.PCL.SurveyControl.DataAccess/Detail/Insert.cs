﻿using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Details;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public partial class Detail
    {
        public static ResponseEmpty Insert(DetailData[] request)
        {
            ResponseEmpty response = new ResponseEmpty();
            if (request == null)
            {
                response.ConfigureNullRequest(1400);
            }
            else if (request.Length < 1)
            {
                response.Configure(Codes.BadRequest, "No se puede insertar un arreglo vacio.", 1415);
            }
            else
            {
                using (var db = new DataBase())
                {
                    if (!db.OpenConnection())
                    {
                        response.ConfigureConnectionFailure();
                    }
                    else
                    {
                        try
                        {
                            foreach (var item in request)
                            {
                                db.Connection.InsertOrReplace(new QuestionDetailTable
                                {
                                    QuestionDetailID = item.EventID,
                                    RouteID = item.RouteID,
                                    SurveyID = item.SurveyID
                                });
                            }
                            response.Configure(Codes.OK);
                        }
                        catch (SQLiteException e)
                        {
                            response.Configure(Codes.Declined, e.Message, 1500);
                        }
                    }
                }
            }
            return response;
        }
    }
}
