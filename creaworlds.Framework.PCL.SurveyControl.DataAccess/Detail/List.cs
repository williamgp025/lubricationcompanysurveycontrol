﻿using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public partial class Detail
    {
        public static ResponseObject<QuestionDetailTable> List(string request)
        {
            ResponseObject<QuestionDetailTable> response = new ResponseObject<QuestionDetailTable>();

            if (string.IsNullOrWhiteSpace(request))
            {
                response.Configure(Codes.BadRequest, "RouteID no puede ser null.", 1415);
            }
            else
            {
                var db = new DataBase();
                if (!db.OpenConnection())
                {
                    response.ConfigureConnectionFailure();
                }
                else
                {
                    try
                    {
                        response.Result = db.Connection.Query<QuestionDetailTable>("SELECT * FROM QuestionDetailTable where RouteID = ?", request).First();
                        response.Configure(Codes.OK);
                    }
                    catch (SQLiteException e)
                    {
                        response.Configure(Codes.Declined, e.Message, 1500);
                    }
                }
            }
            return response;
        }
    }
}
