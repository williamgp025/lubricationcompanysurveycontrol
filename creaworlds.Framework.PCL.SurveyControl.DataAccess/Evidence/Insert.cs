﻿using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Evidences;
using SQLite;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public partial class Evidence
    {
        public static ResponseEmpty Insert(EvidenceTable request)
        {
            ResponseEmpty response = new ResponseEmpty();
            if (request == null)
            {
                response.Configure(Codes.BadRequest, "No se puede insertar un surveyid vacio.", 1410);
            }
            else
            {
                using (var db = new DataBase())
                {
                    if (!db.OpenConnection())
                    {
                        response.ConfigureConnectionFailure();
                    }
                    else
                    {
                        try
                        {
                            db.Connection.Insert(request, typeof(EvidenceTable));
                            response.Configure(Codes.OK);
                        }
                        catch (SQLiteException e)
                        {
                            response.Configure(Codes.Declined, e.Message, 1500);
                        }
                    }
                }
            }
            return response;
        }
    }
}
