﻿using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Evidences;
using System.Linq;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public partial class Evidence
    {
        public static ResponseObject<EvidenceTable[]> List()
        {
            ResponseObject<EvidenceTable[]> response = new ResponseObject<EvidenceTable[]>();
            var db = new DataBase();
            if (!db.OpenConnection())
            {
                response.ConfigureConnectionFailure();
            }
            else
            {
                try
                {
                    response.Result = db.Connection.Table<EvidenceTable>().ToArray();
                    response.Configure(Codes.OK);
                }
                catch (SQLiteException e)
                {
                    response.Configure(Codes.Declined, e.Message, 1500);
                }
            }
            return response;
        }
    }
}
