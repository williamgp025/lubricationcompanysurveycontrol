﻿using System;
using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public static partial class Route
    {
        public static ResponseEmpty Insert(RouteData[] request)
        {
            ResponseEmpty response = new ResponseEmpty();
            if (request == null)
            {
                response.ConfigureNullRequest(1400);
            }
            else if (request.Length < 1)
            {
                response.Configure(Codes.BadRequest, "No se puede insertar un arreglo vacio.", 1415);
            }
            else
            {
                using (var db = new DataBase())
                {
                    if (!db.OpenConnection())
                    {
                        response.ConfigureConnectionFailure();
                    }
                    else
                    {
                        try
                        {
                            if (db.Connection != null)
                            {
                                foreach (var item in request)
                                {
                                    db.Connection.InsertOrReplace(new RouteTable()
                                    {
                                        Address = item.Address,
                                        ID = item.ID,
                                        Key = item.Key,
                                        Latitude = item.Latitude,
                                        Longitude = item.Longitude,
                                        RouteID = item.ID
                                    });
                                }
                                response.Configure(Codes.OK);
                            }
                        }
                        catch (SQLiteException e)
                        {
                            response.Configure(Codes.Declined, e.Message, 1500);
                        }
                    }
                }
            }
            return response;
        }
    }
}
