﻿using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;
using System.Linq;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public static partial class Route
    {
        public static ResponseObject<RouteTable[]> List(bool user = false, bool finish = false)
        {
            ResponseObject<RouteTable[]> response = new ResponseObject<RouteTable[]>();
            var db = new DataBase();

            if (!db.OpenConnection())
            {
                response.ConfigureConnectionFailure();
            }
            else
            {
                try
                {
                    if (user)
                    {
                        response.Result = db.Connection.Table<RouteTable>().ToArray();
                    }
                    else if (finish)
                    {
                        response.Result = db.Connection.Table<RouteTable>().Where(r => r.Finish).ToArray();

                    }
                    else
                    {
                        response.Result = db.Connection.Table<RouteTable>().Where(r => !r.Finish).ToArray();
                    }
                    response.Configure(Codes.OK);
                }
                catch (SQLiteException e)
                {
                    response.Configure(Codes.Declined, e.Message, 1500);
                }
            }
            return response;
        }
    }
}
