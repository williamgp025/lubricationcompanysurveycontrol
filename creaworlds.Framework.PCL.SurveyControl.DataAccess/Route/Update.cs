﻿using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public partial class Route
    {
        public static ResponseEmpty Update(RouteTable request)
        {
            ResponseEmpty response = new ResponseEmpty();
            if (request == null)
            {
                response.ConfigureNullRequest(1400);
            }
            else
            {
                var db = new DataBase();
                if (!db.OpenConnection())
                {
                    response.ConfigureConnectionFailure();
                }
                else
                {
                    try
                    {
                        db.Connection.Query<RouteTable>("UPDATE ROUTETABLE SET FINISH = ?, COMMENT = ? WHERE ROUTEID = ?", request.Finish, request.Comment, request.RouteID);

                        response.Configure(Codes.OK);
                    }
                    catch (SQLiteException e)
                    {
                        response.Configure(Codes.Declined, e.Message, 1500);
                    }
                }
            }
            return response;
        }
    }
}
