﻿using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;
using System.Linq;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public static partial class Question
    {
        public static ResponseObject<QuestionTable[]> List(QuestionTableList request)
        {
            ResponseObject<QuestionTable[]> response = new ResponseObject<QuestionTable[]>();
            if (request == null)
            {
                response.ConfigureNullRequest(1400);
            }
            else if (string.IsNullOrWhiteSpace(request.SurveyID))
            {
                response.Configure(Codes.BadRequest, "SurveyID no puede ser menor que cero.", 1415);
            }
            else
            {
                var db = new DataBase();
                if (!db.OpenConnection())
                {
                    response.ConfigureConnectionFailure();
                }
                else
                {
                    try
                    {
                        //response.Result = db.Connection.Query<QuestionTable>("SELECT * FROM QuestionTable where SurveyID =?", request.SurveyID).ToArray();
                        response.Result = db.Connection.Table<QuestionTable>().Where(q => q.SurveyID.Equals(request.SurveyID)).ToArray();
                        response.Configure(Codes.OK);
                    }
                    catch (SQLiteException e)
                    {
                        response.Configure(Codes.Declined, e.Message, 1500);
                    }
                }
            }
            return response;
        }
    }
}
