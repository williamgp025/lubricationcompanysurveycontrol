﻿using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public static partial class Question
    {
        public static ResponseEmpty Insert(QuestionData[] request)
        {
            ResponseEmpty response = new ResponseEmpty();
            if (request == null)
            {
                response.ConfigureNullRequest(1400);
            }
            else if (request.Length < 1)
            {
                response.Configure(Codes.BadRequest, "No se puede insertar un arreglo vacio.", 1415);
            }
            else
            {
                using (var db = new DataBase())
                {
                    if (!db.OpenConnection())
                    {
                        response.ConfigureConnectionFailure();
                    }
                    else
                    {
                        try
                        {
                            foreach (var item in request)
                            {

                                db.Connection.InsertOrReplace(new QuestionTable()
                                {
                                    Description = item.Description,
                                    Help = item.Help,
                                    ID = item.ID,
                                    IsMore = item.IsMore,
                                    Order = item.Order,
                                    SurveyID = item.SurveyID
                                });
                            }
                            response.Configure(Codes.OK);
                        }
                        catch (SQLiteException e)
                        {
                            response.Configure(Codes.Declined, e.Message, 1500);
                        }
                    }
                }
            }
            return response;
        }
    }
}
