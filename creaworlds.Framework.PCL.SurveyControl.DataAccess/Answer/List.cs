﻿using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Answers;
using System.Linq;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public partial class Answer
    {
        public static ResponseObject<AnswerTable[]> List(string request)
        {
            ResponseObject<AnswerTable[]> response = new ResponseObject<AnswerTable[]>();

            if (string.IsNullOrWhiteSpace(request))
            {
                response.Configure(Codes.BadRequest, "El ID no debe ser vacio", 1400);
            }
            else
            {
                using (var db = new DataBase())
                {
                    if (!db.OpenConnection())
                    {
                        response.ConfigureConnectionFailure();
                    }
                    else
                    {
                        try
                        {
                            response.Result = db.Connection.Query<AnswerTable>("SELECT * FROM AnswerTable where QuestionDetailID = ?", request).ToArray();

                            if (response.Result != null)
                            {
                                response.Configure(Codes.OK);
                            }
                        }
                        catch (SQLiteException e)
                        {
                            response.Configure(Codes.Declined, e.Message, 1500);
                        }
                    }
                }
            }
            return response;
        }
    }
}
