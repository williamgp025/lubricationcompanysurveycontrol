﻿using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Answers;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public static partial class Answer
    {
        public static ResponseEmpty Update(AnswerTable request)
        {
            ResponseEmpty response = new ResponseEmpty();
            if (request == null)
            {
                response.ConfigureNullRequest(1400);
            }
            else
            {
                using (var db = new DataBase())
                {
                    if (!db.OpenConnection())
                    {
                        response.ConfigureConnectionFailure();
                    }
                    else
                    {
                        try
                        {
                            db.Connection.Update(request, typeof(AnswerTable));
                            response.Configure(Codes.OK);
                        }
                        catch (SQLiteException e)
                        {
                            response.Configure(Codes.Declined, e.Message, 1500);
                        }
                    }
                }
            }
            return response;
        }
    }
}
