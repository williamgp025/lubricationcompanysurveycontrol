﻿using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Answers;
using System.Linq;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public partial class Answer
    {
        public static ResponseEmpty Insert(AnswerTable request)
        {
            ResponseEmpty response = new ResponseEmpty();
            if (request == null)
            {
                response.ConfigureNullRequest(1400);
            }
            else
            {
                using (var db = new DataBase())
                {
                    if (!db.OpenConnection())
                    {
                        response.ConfigureConnectionFailure();
                    }
                    else
                    {
                        try
                        {
                            var tem = from t1 in db.Connection.Table<AnswerTable>().ToList()
                                      where t1.QuestionDetailID.Equals(request.QuestionDetailID)
                                      && t1.QuestionID.Equals(request.QuestionID)
                                      select t1; 
                            if (tem.Count() > 0)
                            {
                                request.PK = tem.FirstOrDefault().PK;
                                db.Connection.Update(request, typeof(AnswerTable));
                            }else
                                db.Connection.Insert(request, typeof(AnswerTable));
                            response.Configure(Codes.OK);
                        }
                        catch (SQLiteException e)
                        {
                            response.Configure(Codes.Declined, e.Message, 1500);
                        }

                    }
                }
            }
            return response;
        }
    }
}
