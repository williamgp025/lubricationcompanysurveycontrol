﻿using Android.Database.Sqlite;
using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Answers;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;
using creaworlds.Framework.PCL.SurveyControl.Entities.Surveys;
using System.Linq;
using System.Text;

namespace creaworlds.Framework.PCL.SurveyControl.DataAccess
{
    public partial class Answer
    {
        public static ResponseObject<AnswerTable> Find(string routeid, string questionid)
        {
            ResponseObject<AnswerTable> response = new ResponseObject<AnswerTable>();
            if (string.IsNullOrWhiteSpace(routeid))
            {
                response.Configure(Codes.BadRequest, "Debe especificar el RouteID", 1400);
            }
            else if (string.IsNullOrWhiteSpace(questionid))
            {
                response.Configure(Codes.BadRequest, "Debe especificar el QuestionID", 1405);
            }
            else
            {
                using (var db = new DataBase())
                {
                    if (!db.OpenConnection())
                    {
                        response.ConfigureConnectionFailure();
                    }
                    else
                    {
                        try
                        {
                            var tem = from t1 in db.Connection.Table<AnswerTable>().ToList()
                                      join t2 in db.Connection.Table<QuestionDetailTable>().ToList() on t1.QuestionDetailID equals t2.QuestionDetailID
                                      join t3 in db.Connection.Table<SurveyTable>().ToList() on t2.SurveyID equals t3.ID
                                      join t4 in db.Connection.Table<RouteTable>().ToList() on t2.RouteID equals t4.ID
                                      join t5 in db.Connection.Table<QuestionTable>().ToList() on t1.QuestionID equals t5.ID
                                      where t4.ID.Equals(routeid) && t5.ID.Equals(questionid)
                                      select t1;                                      

                            response.Result = tem.FirstOrDefault();

                            if (response.Result != null)
                            {
                                response.Configure(Codes.OK);
                            }
                        }
                        catch (SQLiteException e)
                        {
                            response.Configure(Codes.Declined, e.Message, 1500);
                        }
                    }
                }
            }
            return response;
        }
    }
}
