﻿namespace creaworlds.Framework.PCL.SurveyControl.Interfaces
{
    public interface ISignatureable
    {
        object Signature { get; set; }
    }
}
