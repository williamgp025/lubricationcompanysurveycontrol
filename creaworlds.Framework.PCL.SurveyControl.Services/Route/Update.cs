﻿using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;
using creaworlds.Framework.PCL.SurveyControl.Entities.Users;
using creaworlds.Framework.PCL.SurveyControl.Extensions;
using System.Threading.Tasks;

namespace creaworlds.Framework.PCL.SurveyControl.Services
{
    public static partial class Route
    {
        public static async Task<ResponseEmpty> Update(RouteUpdate request, UserData User)
        {
            ResponseEmpty response = new ResponseEmpty();

            if (request == null)
            {
                response.ConfigureNullContent(1400);
            }
            else
            {
                response = await Service.GetDataRestAsync(User.GetSignature(request), "Route", "Update");
                response.Configure(Codes.OK);
            }
            return response;
        }
    }
}
