﻿using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Routes;
using creaworlds.Framework.PCL.SurveyControl.Entities.Users;
using creaworlds.Framework.PCL.SurveyControl.Extensions;
using System.Threading.Tasks;

namespace creaworlds.Framework.PCL.SurveyControl.Services
{
    public static partial class Route
    {
        public static async Task<ResponseObject<RouteContent>> List(UserData User, RouteList request)
        {
            var response = new ResponseObject<RouteContent>();

            if (response.ConfigureInvalidSignature(User))
            {
                if (request == null)
                {
                    response.ConfigureNullRequest(1415);
                }
                else if (string.IsNullOrWhiteSpace(request.UserID))
                {
                    response.Configure(Codes.BadRequest, "Falta UserID", 1420);
                }
                else
                {
                    response = await Service.GetDataRestAsync<RouteContent>( User.GetSignature(request), "Route", "List");
                }
            }
            return response;
        }
    }
}
