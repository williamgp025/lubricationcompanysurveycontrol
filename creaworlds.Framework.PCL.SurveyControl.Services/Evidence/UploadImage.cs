﻿using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Evidences;
using creaworlds.Framework.PCL.SurveyControl.Entities.Users;
using creaworlds.Framework.PCL.SurveyControl.Extensions;
using System.IO;
using System.Threading.Tasks;

namespace creaworlds.Framework.PCL.SurveyControl.Services
{
    public partial class Evidence
    {
        public static async Task<ResponseEmpty> UploadImage(EvidenceTable request, UserData User)
        {
            var response = new ResponseEmpty();
            if (response.ConfigureInvalidSignature(User))
            {
                if (request == null)
                {
                    response.ConfigureNullRequest(1415);
                }
                else if (string.IsNullOrWhiteSpace(request.FilePath))
                {
                    response.Configure(Codes.BadRequest,"El registro no tiene una ruta fisica asignada",1420);
                }
                else
                {
                    Java.IO.File file;
                    var reader = new Java.IO.FileInputStream(file = new Java.IO.File(request.FilePath));
                    byte[] img = new byte[(int)file.Length()];
                    reader.Read(img,0,(int)file.Length());

                    response = await Service.GetDataRestAsync(User.GetSignature(new EvidenceInsert
                    {
                        FileContent = img,
                        FileName = Path.GetFileName(request.FilePath),
                        ContentLength = request.ContentLength,
                        ContentType = Path.GetExtension(request.FilePath),
                        EventID = request.QuestionDetailID,
                        ImgLatitude = request.Latitude,
                        ImgLongitude = request.Longitude
                    }), "Evidence", "Insert");
                    
                }
            }
            return response;
        }
    }
}
