﻿using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Evidences;
using creaworlds.Framework.PCL.SurveyControl.Entities.Users;
using creaworlds.Framework.PCL.SurveyControl.Extensions;
using System.Threading.Tasks;

namespace creaworlds.Framework.PCL.SurveyControl.Services
{
    public static partial class Evidence
    {
        public static async Task<ResponseEmpty> Insert(UserData User, EvidenceInsert request)
        {
            var response = new ResponseEmpty();

            if (response.ConfigureInvalidSignature(User))
            {
                if (request == null)
                {
                    response.ConfigureNullRequest(1415);
                }
                else
                {
                    response = await Service.GetDataRestAsync(User.GetSignature(request)
                        , "Customer", "Login");
                }
            }
            return response;
        }
    }
}
