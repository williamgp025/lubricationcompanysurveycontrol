﻿using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Enumerators.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Questions;
using creaworlds.Framework.PCL.SurveyControl.Entities.Users;
using System.Threading.Tasks;
using creaworlds.Framework.PCL.SurveyControl.Extensions;


namespace creaworlds.Framework.PCL.SurveyControl.Services
{
    public static partial class Question
    {
        public static async Task<ResponseObject<QuestionData[]>> List(UserData User, QuestionList request)
        {
            var response = new ResponseObject<QuestionData[]>();
            if (response.ConfigureInvalidSignature(User))
            {
                if (request == null)
                {
                    response.ConfigureNullRequest(1400);
                }
                else if (string.IsNullOrWhiteSpace(request.ID))
                {
                    response.Configure(Codes.BadRequest, "No especifico UserID", 1415);
                }
                else
                {

                    response = await Service.GetDataRestAsync<QuestionData[]>(
                    User.GetSignature(request),
                    "Customer", "Login");
                }
            }
            return response;
        }
    }
}
