﻿using creaworlds.Framework.PCL.Core.Entities.Responses;
using creaworlds.Framework.PCL.Core.Tools;
using creaworlds.Framework.PCL.SurveyControl.Entities.Users;
using creaworlds.Framework.PCL.SurveyControl.Extensions;
using System.Threading.Tasks;

namespace creaworlds.Framework.PCL.SurveyControl.Services
{
    public static partial class User
    {
        public static async Task<ResponseObject<UserData>> Login(string UserName, string Password)
        {
            var response = new ResponseObject<UserData>();
            if (string.IsNullOrWhiteSpace(UserName))
            {
                response.ConfigureInvalidUserName(1410);
            }
            else if (string.IsNullOrWhiteSpace(Password))
            {
                response.ConfigureInvalidPassword(1420);
            }
            else
            {
                response = await Service.GetDataRestAsync<UserData>(new UserLogin { UserName = UserName, Password = Password }
                , "User", "Login");
            }
            return response;
        }
    }
}
